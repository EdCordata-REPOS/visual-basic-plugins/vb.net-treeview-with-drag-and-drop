﻿Imports System.Runtime.InteropServices


Public Class DragAndDropTreeView
    Inherits System.Windows.Forms.TreeView


    ' public properties & events to configure drag & drop features
    ' ===========================================================================================

    Public Property DragAndDrop_Enable                  As Boolean = False
    Public Property DragAndDrop_ItemHeight              As Integer = 20
    Public Property DragAndDrop_ItemDropTopOffset       As Integer = 6
    Public Property DragAndDrop_ItemDropBotOffset       As Integer = 6
    Public Property DragAndDrop_ItemDropArrowLeftOffset As Integer = 8
    Public Property DragAndDrop_ItemDropArrowTopOffset  As Integer = 2
    Public Property DragAndDrop_ItemDropLineHeight      As Integer = 2
    Public Property DragAndDrop_ItemDropLineWidth       As Integer = 100
    Public Property DragAndDrop_ItemDropLineLeftOffset  As Integer = 0
    Public Property DragAndDrop_ItemDropLineTopOffset   As Integer = 0
    Public Property DragAndDrop_ItemDropLineBotOffset   As Integer = 0
    Public Property DragAndDrop_CursorDefault           As Cursor  = Cursors.Default
    Public Property DragAndDrop_CursorOnDrag            As Cursor  = Cursors.Hand
    Public Property DragAndDrop_CursorOnNotAllowed      As Cursor  = Cursors.No
    Public Property DragAndDrop_KeepExpanded            As Boolean = True
    Public Property DragAndDrop_DragHandleColor         As Brush   = Brushes.DarkRed

    Public Event DragAndDrop_CanDrag(ByRef data As Object)
    Public Event DragAndDrop_CanDrop(ByRef data As Object)

    Public Event DragAndDrop_OnDrop(ByVal data As Object)

    ' ===========================================================================================


    ' events
    ' ===========================================================================================

    Private Function DragAndDrop_CanDragCall(node As TreeNode) As Boolean
        Dim data = New With { Key .node = node, .response = True}

        If DragAndDrop_CanDragEvent IsNot Nothing Then
            RaiseEvent DragAndDrop_CanDrag(data)
        End If

        Return data.response
    End Function


    Private Function DragAndDrop_CanDropCall(drag_node As TreeNode, drop_node As TreeNode) As Boolean
        Dim data = New With { .drag_node = drag_node, .drop_node = drop_node, .response = True}

        If DragAndDrop_CanDropEvent IsNot Nothing Then
            RaiseEvent DragAndDrop_CanDrop(data)
        End If

        Return Data.response
    End Function


    Private Sub DragAndDrop_OnDropCall(drag_node As TreeNode, drop_node As TreeNode)
        Dim data = New With { .drag_node = drag_node, .drop_node = drop_node }

        If DragAndDrop_OnDropEvent IsNot Nothing Then
            RaiseEvent DragAndDrop_OnDrop(data)
        End If
    End Sub

    ' ===========================================================================================


    ' fix flicker by using double buffer
    ' ===========================================================================================

    Private Const TVM_SETEXTENDEDSTYLE As Integer = &H1100 + 44
    Private Const TVS_EX_DOUBLEBUFFER  As Integer = &H4

    Protected Overrides Sub OnHandleCreated(ByVal e As EventArgs)
        SendMessage(Me.Handle, TVM_SETEXTENDEDSTYLE, CType(TVS_EX_DOUBLEBUFFER, IntPtr), CType(TVS_EX_DOUBLEBUFFER, IntPtr))
        MyBase.OnHandleCreated(e)
    End Sub

    <DllImport("user32.dll")>
    Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wp As IntPtr, ByVal lp As IntPtr) As IntPtr
    End Function

    ' ===========================================================================================


    ' Private variables
    ' ===========================================================================================

    Dim _DragAndDrop_drag_node      As TreeNode = Nothing
    Dim _DragAndDrop_draw_pos       As String   = Nothing
    Dim _DragAndDrop_draw_node      As TreeNode = Nothing
    Dim _DragAndDrop_draw_last_pos  As String   = Nothing
    Dim _DragAndDrop_draw_last_node As TreeNode = Nothing

    ' ===========================================================================================


    ' initialize instance
    ' ===========================================================================================

    Public Sub New()
        Me.ItemHeight = DragAndDrop_ItemHeight

        AddHandler Me.MouseUp,      AddressOf DragAndDrop_OnMouseUp
        AddHandler Me.MouseDown,    AddressOf DragAndDrop_OnMouseDown
        AddHandler Me.MouseMove,    AddressOf DragAndDrop_OnMouseMove
        AddHandler Me.MouseLeave,   AddressOf DragAndDrop_OnMouseLeave
        AddHandler Me.AfterSelect,  AddressOf DragAndDrop_OnSelectChanged
    End Sub

    ' ===========================================================================================


    ' events
    ' ===========================================================================================

    Private Sub DragAndDrop_OnMouseDown(sender As Object, e As MouseEventArgs)
        Dim node As TreeNode = DragAndDrop_GetNodeUnderMouse(e)

        ' fix selection jumping back to original on mouse down
        If node IsNot Nothing Then Me.SelectedNode = node

        ' exit if drag & drop disabled
        If DragAndDrop_Enable = False Then return

        ' check drag status
        If node IsNot Nothing Then
            If DragAndDrop_CanDragCall(node) = True Then
                _DragAndDrop_drag_node = node
                CheckDrag(e)
            End If
        End If
    End Sub


    Private Sub DragAndDrop_OnMouseUp(sender As Object, e As MouseEventArgs)
        If DragAndDrop_Enable = False Then Return

        If _DragAndDrop_drag_node IsNot Nothing And _DragAndDrop_draw_last_pos IsNot Nothing Then

            Dim drop_node As TreeNode = DragAndDrop_GetNodeUnderMouse(e)

            ' clone drag node
            ' -----------------------------
            Dim drag_node = _DragAndDrop_drag_node.Clone()
            ' -----------------------------

            ' if dragging on an item
            ' -----------------------------
            If _DragAndDrop_draw_last_pos = "mid" Then
                If Not DragAndDrop_GetParentNodesPath(drop_node).Contains(_DragAndDrop_drag_node) Then
                    If DragAndDrop_CanDropCall(_DragAndDrop_drag_node, drop_node) Then
                        drop_node.Nodes.Add(drag_node)
                        _DragAndDrop_drag_node.Remove()
                        DragAndDrop_OnDropCall(drag_node, drop_node)
                    End If
                End If
            End If
            ' -----------------------------

            ' if dragging on an items top-side
            ' -----------------------------
            If _DragAndDrop_draw_last_pos = "top" Then
                If Not DragAndDrop_GetParentNodesPath(drop_node).Contains(_DragAndDrop_drag_node) Then
                    If drop_node.Parent Is Nothing Then
                        Me.Nodes.Insert(drop_node.Index, drag_node)
                        DragAndDrop_OnDropCall(drag_node, Nothing)
                    Else
                        drop_node.Parent.Nodes.Insert(drop_node.Index, drag_node)
                        DragAndDrop_OnDropCall(drag_node,  drop_node.Parent)
                    End If

                    _DragAndDrop_drag_node.Remove()
                End If
            End If
            ' -----------------------------

            ' if dragging on an items bot-side
            ' -----------------------------
            If _DragAndDrop_draw_last_pos = "bot" Then
                If Not DragAndDrop_GetParentNodesPath(drop_node).Contains(_DragAndDrop_drag_node) Then
                    If drop_node Is Nothing Then
                        Me.Nodes.Insert(Me.Nodes.Count, drag_node)
                    Else
                        If drop_node.Parent Is Nothing Then
                            Me.Nodes.Insert(drop_node.Index + 1, drag_node)
                            DragAndDrop_OnDropCall(drag_node, Nothing)
                        Else
                            drop_node.Parent.Nodes.Insert(drop_node.Index + 1, drag_node)
                            DragAndDrop_OnDropCall(drag_node,  drop_node.Parent)
                        End If
                    End If

                    _DragAndDrop_drag_node.Remove()
                End If
            End If
            ' -----------------------------

        End If

        DragAndDrop_StopDrag()
    End Sub


    Private Sub DragAndDrop_OnMouseMove(sender As Object, e As MouseEventArgs)
        If DragAndDrop_Enable = False Then Return
        If _DragAndDrop_drag_node IsNot Nothing Then CheckDrag(e)
    End Sub


    Private Sub DragAndDrop_OnMouseLeave(sender As Object, e As EventArgs)
        DragAndDrop_StopDrag()
    End Sub


    Private Sub DragAndDrop_OnSelectChanged(sender As Object, e As TreeViewEventArgs)
        If DragAndDrop_KeepExpanded = True Then Me.ExpandAll()
    End Sub

    ' ===========================================================================================


    ' private functions
    ' ===========================================================================================

    Private Sub CheckDrag(e As MouseEventArgs)
        Dim drop_node As TreeNode = DragAndDrop_GetNodeUnderMouse(e)

        _DragAndDrop_draw_pos  = Nothing

        ' set default drag cursor
        Dim cursor As Cursor = DragAndDrop_CursorOnDrag

        ' if node not found under mouse
        ' -----------------------------
        If drop_node Is Nothing Then
            Dim last_node As TreeNode = Nothing

            For Each node As TreeNode In Me.Nodes
                last_node = node
            Next

            _DragAndDrop_draw_node = last_node
            _DragAndDrop_draw_pos  = "bot"
        End If
        ' -----------------------------


        ' if node found under mouse
        ' -----------------------------
        If drop_node IsNot Nothing Then
            _DragAndDrop_draw_node = drop_node

            ' get drag pos on node under the mouse (top/mid/bot)
            Dim drop_pos As String = DragAndDrop_GetDropPos(e, drop_node)

            If drop_pos = "mid" Then
                 If Not DragAndDrop_GetParentNodesPath(drop_node).Contains(_DragAndDrop_drag_node) Then
                    If DragAndDrop_CanDropCall(_DragAndDrop_drag_node, drop_node) Then
                        _DragAndDrop_draw_pos  = "mid"
                    End If
                 End If
            End If

            If drop_pos = "top" Or drop_pos = "bot" Then
                 If Not DragAndDrop_GetParentNodesPath(drop_node).Contains(_DragAndDrop_drag_node) Then
                    _DragAndDrop_draw_pos  = drop_pos
                 End If
            End If

        End If
        ' -----------------------------

        ' Handle cursors & draw
        ' -----------------------------
        If _DragAndDrop_draw_pos Is Nothing Then
            cursor = DragAndDrop_CursorOnNotAllowed
        Else
            cursor = DragAndDrop_CursorOnDrag
        End If

        DragAndDrop_HandleDraw()
        Me.FindForm.Cursor = cursor
        ' -----------------------------

    End Sub


    Private Sub DragAndDrop_StopDrag()
        _DragAndDrop_drag_node     = Nothing
        _DragAndDrop_draw_node     = Nothing
        _DragAndDrop_draw_pos      = Nothing
        _DragAndDrop_draw_last_pos = Nothing
        _DragAndDrop_draw_last_pos = Nothing
        Me.Refresh()
        Me.FindForm.Cursor = DragAndDrop_CursorDefault
    End Sub


    Private Sub DragAndDrop_HandleDraw()
        If _DragAndDrop_draw_pos Is Nothing Then
            If _DragAndDrop_draw_last_pos IsNot Nothing Then
                Me.Refresh()
                _DragAndDrop_draw_last_pos = Nothing
            End If
        Else
            If _DragAndDrop_draw_last_node Is _DragAndDrop_draw_node And _DragAndDrop_draw_last_pos = _DragAndDrop_draw_pos Then
            Else
                _DragAndDrop_draw_last_node = _DragAndDrop_draw_node
                _DragAndDrop_draw_last_pos  = _DragAndDrop_draw_pos

                If _DragAndDrop_draw_node Is Nothing Then
                    Me.Refresh()
                Else

                    If _DragAndDrop_draw_pos = "top" Then
                        DragAndDrop_HandleDrawTopLine()
                    ElseIf _DragAndDrop_draw_pos = "bot" Then
                        DragAndDrop_HandleDrawBotLine()
                    Else
                        DragAndDrop_HandleDrawArrow()
                    End If
                End If
            End If
        End If
    End Sub


    Private Sub DragAndDrop_HandleDrawTopLine
        Dim dragged_node_line_start_pos As Point = New Point(
            _DragAndDrop_draw_node.Bounds.X + DragAndDrop_ItemDropLineLeftOffset,
            _DragAndDrop_draw_node.Bounds.Y - DragAndDrop_ItemDropLineTopOffset
        )

        Dim dragged_node_line_end_pos As Point = dragged_node_line_start_pos
        dragged_node_line_end_pos.X = dragged_node_line_end_pos.X + DragAndDrop_ItemDropLineWidth

        Me.Refresh()

        Me.CreateGraphics.DrawLine(
            New Pen(DragAndDrop_DragHandleColor, DragAndDrop_ItemDropLineHeight),
            dragged_node_line_start_pos,
            dragged_node_line_end_pos
        )
    End Sub


    Private Sub DragAndDrop_HandleDrawBotLine
        Dim dragged_node_line_start_pos As Point = New Point(
            _DragAndDrop_draw_node.Bounds.X + DragAndDrop_ItemDropLineLeftOffset,
            _DragAndDrop_draw_node.Bounds.Y + _DragAndDrop_draw_node.Bounds.Height + DragAndDrop_ItemDropLineBotOffset
        )

        Dim dragged_node_line_end_pos As Point = dragged_node_line_start_pos
        dragged_node_line_end_pos.X = dragged_node_line_end_pos.X + DragAndDrop_ItemDropLineWidth

        Me.Refresh()

        Me.CreateGraphics.DrawLine(
            New Pen(DragAndDrop_DragHandleColor, DragAndDrop_ItemDropLineHeight),
            dragged_node_line_start_pos,
            dragged_node_line_end_pos
        )
    End Sub


    Private Sub DragAndDrop_HandleDrawArrow()
        Me.Refresh()

        Dim arrow_pos As Point = New Point(
            _DragAndDrop_draw_node.Bounds.Right  + DragAndDrop_ItemDropArrowLeftOffset,
            _DragAndDrop_draw_node.Bounds.Height + DragAndDrop_ItemDropArrowTopOffset
        )

        Dim RightTriangle As Point() = New Point(4) {
            New Point( arrow_pos.X,     _DragAndDrop_draw_node.Bounds.Y + (arrow_pos.Y / 2) + 4 ),
            New Point( arrow_pos.X,     _DragAndDrop_draw_node.Bounds.Y + (arrow_pos.Y / 2) + 4 ),
            New Point( arrow_pos.X - 4, _DragAndDrop_draw_node.Bounds.Y + (arrow_pos.Y / 2)     ),
            New Point( arrow_pos.X - 4, _DragAndDrop_draw_node.Bounds.Y + (arrow_pos.Y / 2) - 1 ),
            New Point( arrow_pos.X,     _DragAndDrop_draw_node.Bounds.Y + (arrow_pos.Y / 2) - 5 )
        }

        Me.CreateGraphics.FillPolygon(DragAndDrop_DragHandleColor, RightTriangle)
    End Sub


    Private Function DragAndDrop_GetNodeUnderMouse(e As MouseEventArgs) As TreeNode
        Return Me.GetNodeAt(New Point(e.X, e.Y))
    End Function


    Private Function DragAndDrop_GetDropPos(e As MouseEventArgs, node As TreeNode) As String
        Dim node_pos_y  As Integer = node.Bounds.Y
        Dim node_height As Integer = node.Bounds.Height
        Dim mouse_pos_y As Integer = e.Y
        Dim pos         As String  = "mid"

        If ( mouse_pos_y < (node_pos_y + DragAndDrop_ItemDropTopOffset)               ) Then pos = "top"
        If ( mouse_pos_y > (node_pos_y + node_height - DragAndDrop_ItemDropBotOffset) ) Then pos = "bot"

        Return pos
    End Function


    Private Function DragAndDrop_GetParentNodesPath(node As TreeNode) As List(Of TreeNode)
        If node Is Nothing Then Return New List(Of TreeNode)

        Dim nodes     As List(Of TreeNode) = New List(Of TreeNode)
        Dim curr_node As TreeNode          = node

        nodes.Add(node)

        While curr_node.Parent IsNot Nothing
            nodes.Add(node.Parent)
            curr_node = curr_node.Parent
        End While

        Return nodes
    End Function

    ' ===========================================================================================


End Class
