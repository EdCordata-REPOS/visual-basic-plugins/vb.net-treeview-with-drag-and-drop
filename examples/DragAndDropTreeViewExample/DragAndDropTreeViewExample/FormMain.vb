﻿Public Class FormMain

    Private Sub FormMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ' add icons
        ' ----------------------------------------
        TreeViewMain.ImageList = New ImageList

        TreeViewMain.ImageList.Images.Add(My.Resources.folder)
        TreeViewMain.ImageList.Images.Add(My.Resources.file)
        ' ----------------------------------------

        ' Configure TreeView
        ' ----------------------------------------

        ' make line start with the icon (icon pushes text to the right)
        TreeViewMain.DragAndDrop_ItemDropLineLeftOffset = -16

        ' ----------------------------------------

        ' add items
        ' ----------------------------------------
        Dim item1 As TreeNode = New TreeNode
        item1.Text               = "Folder 1"
        item1.Tag                = "1"
        item1.ImageIndex         = 0
        item1.SelectedImageIndex = 0
        TreeViewMain.Nodes.Add(item1)

        Dim item2 As TreeNode = New TreeNode
        item2.Text               = "Folder 2"
        item2.Tag                = "2"
        item2.ImageIndex         = 0
        item2.SelectedImageIndex = 0
        TreeViewMain.Nodes.Add(item2)

        Dim item3 As TreeNode = New TreeNode
        item3.Text               = "Folder 3"
        item3.Tag                = "3"
        item3.ImageIndex         = 0
        item3.SelectedImageIndex = 0
        TreeViewMain.Nodes.Add(item3)

        Dim item4 As TreeNode = New TreeNode
        item4.Text               = "File 1"
        item4.Tag                = "4"
        item4.ImageIndex         = 1
        item4.SelectedImageIndex = 1
        item3.Nodes.Add(item4)

        Dim item5 As TreeNode = New TreeNode
        item5.Text               = "File 2"
        item5.Tag                = "5"
        item5.ImageIndex         = 1
        item5.SelectedImageIndex = 1
        TreeViewMain.Nodes.Add(item5)
        ' ----------------------------------------

        TreeViewMain.ExpandAll()
    End Sub


    Private Sub TreeViewMain_DragAndDrop_CanDrag(ByRef data As Object) Handles TreeViewMain.DragAndDrop_CanDrag
        ' data.drag_node ' TreeNode that is getting dragged
        data.response = True
    End Sub

    Private Sub TreeViewMain_DragAndDrop_CanDrop(ByRef data As Object) Handles TreeViewMain.DragAndDrop_CanDrop
        ' data.drag_node ' TreeNode that is getting dragged
        ' data.drop_node ' TreeNode that is under mouse and would be dropped on, if mouse is released
        If data.drop_node.Text.Contains("File") Then data.response = False
    End Sub

    Private Sub TreeViewMain_DragAndDrop_OnDrop(data As Object) Handles TreeViewMain.DragAndDrop_OnDrop
        ' data.drag_node ' TreeNode that is getting dragged
        ' data.drop_node ' TreeNode that drag_node got dropped on. It will be 'Nothing' if dropped on root element
        If data.drop_node Is Nothing Then
            Debug.WriteLine("OnDrop: " + data.drag_node.Tag + " -> root")
        Else
            Debug.WriteLine("OnDrop: " + data.drag_node.Tag + " -> " + data.drop_node.Tag)
        End If
    End Sub

End Class
