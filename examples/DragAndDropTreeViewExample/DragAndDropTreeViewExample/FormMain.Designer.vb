﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TreeViewMain = New DragAndDropTreeViewExample.DragAndDropTreeView()
        Me.SuspendLayout
        '
        'TreeViewMain
        '
        Me.TreeViewMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeViewMain.DragAndDrop_CursorDefault = System.Windows.Forms.Cursors.Default
        Me.TreeViewMain.DragAndDrop_CursorOnDrag = System.Windows.Forms.Cursors.Hand
        Me.TreeViewMain.DragAndDrop_CursorOnNotAllowed = System.Windows.Forms.Cursors.No
        Me.TreeViewMain.DragAndDrop_Enable = true
        Me.TreeViewMain.DragAndDrop_ItemDropArrowLeftOffset = 8
        Me.TreeViewMain.DragAndDrop_ItemDropArrowTopOffset = 2
        Me.TreeViewMain.DragAndDrop_ItemDropBotOffset = 6
        Me.TreeViewMain.DragAndDrop_ItemDropLineBotOffset = 0
        Me.TreeViewMain.DragAndDrop_ItemDropLineHeight = 2
        Me.TreeViewMain.DragAndDrop_ItemDropLineLeftOffset = 0
        Me.TreeViewMain.DragAndDrop_ItemDropLineTopOffset = 0
        Me.TreeViewMain.DragAndDrop_ItemDropLineWidth = 100
        Me.TreeViewMain.DragAndDrop_ItemDropTopOffset = 6
        Me.TreeViewMain.DragAndDrop_ItemHeight = 20
        Me.TreeViewMain.DragAndDrop_KeepExpanded = true
        Me.TreeViewMain.ItemHeight = 20
        Me.TreeViewMain.Location = New System.Drawing.Point(0, 0)
        Me.TreeViewMain.Name = "TreeViewMain"
        Me.TreeViewMain.ShowPlusMinus = false
        Me.TreeViewMain.Size = New System.Drawing.Size(242, 248)
        Me.TreeViewMain.TabIndex = 0
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(242, 248)
        Me.Controls.Add(Me.TreeViewMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Drag & Drop TreeView Example"
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents TreeViewMain As DragAndDropTreeView
End Class
