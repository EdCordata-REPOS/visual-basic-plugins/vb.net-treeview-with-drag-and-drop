# VB.Net TreeView with Drag & Drop
This is a custom component, that inherits from `TreeView`,
and adds Drag & Drop features, that are easy to
customize with properties and events. It works just like
regular `TreeView`, except it has added Drag & Drop features.

![Example](_DOKI/example.gif)

There is an example .Net project and even built exe file in
the [examples](/examples) folder.



## Installation
1) Copy
   [DragAndDropTreeView.vb](src/DragAndDropTreeView.vb)
   anywhere in your project.
2) Add `DragAndDropTreeView` to your form, just like you would regular `TreeView`.


If your Visual Studio doesn't show a component called `DragAndDropTreeView` in
the toolbox, just build the project (`Build` -> `Build Solution`).

If that doesn't work,
then you can import regular `TreeView` and then manually set to use
`DragAndDropTreeView` in the `FormMain.InitializeComponent`, like so:
```vb
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain

 'NOTE: The following procedure is required by the Windows Form Designer
 'It can be modified using the Windows Form Designer.  
 'Do not modify it using the code editor.
 <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()

      Me.TreeViewMain = New System.Windows.Forms.TreeView()
      ' replace above to bellow
      Me.TreeViewMain = New DragAndDropTreeView()

   End Sub

End Class
```



## Configuration
You can configure the Drag & Drop features, with properties and events.

### Properties:
| Property                                 | Type            | Default           | Description
| --- | --- | --- | ---
| `DragAndDrop_Enable`                  | `Boolean` | `False`           | Enable drag & drop features
| `DragAndDrop_ItemHeight`              | `Integer` | `20`              | Set's the default height of an `TreeNode`. Overwrites default `TreeView.ItemHeight`
| `DragAndDrop_ItemDropTopOffset`       | `Integer` | `6`               | Offset from `TreeNode` top in px to calculate when to drop `TreeNode` above the one bellow the mouse
| `DragAndDrop_ItemDropBotOffset`       | `Integer` | `6`               | Offset from `TreeNode` top in px to calculate when to drop `TreeNode` bellow the one bellow the mouse
| `DragAndDrop_ItemDropArrowLeftOffset` | `Integer` | `8`               | Drag arrow's left offset, relative to `TreeNode`, while dragging
| `DragAndDrop_ItemDropArrowTopOffset`  | `Integer` | `2`               | Drag arrow's top offset, relative to `TreeNode`, while dragging
| `DragAndDrop_ItemDropLineHeight`      | `Integer` | `2`               | Height of the drag line above/bellow the `TreeNode`, while dragging
| `DragAndDrop_ItemDropLineWidth`       | `Integer` | `100`             | Width of the drag line above/bellow the `TreeNode`, while dragging
| `DragAndDrop_ItemDropLineLeftOffset`  | `Integer` | `0`               | Drag line's space from left, relative to `TreeNode`, while dragging
| `DragAndDrop_ItemDropLineTopOffset`   | `Integer` | `0`               | Drag line's space from top, relative to `TreeNode`, while dragging
| `DragAndDrop_ItemDropLineBotOffset`   | `Integer` | `0`               | Drag line's space from bot, relative to `TreeNode`, while dragging
| `DragAndDrop_CursorDefault`           | `Cursor`  | `Cursors.Default` | Default cursor
| `DragAndDrop_CursorOnDrag`            | `Cursor`  | `Cursors.Hand`    | Cursor, while dragging
| `DragAndDrop_CursorOnNotAllowed`      | `Cursor`  | `Cursors.No`      | Cursor, while drop is not allowed, while dragging
| `DragAndDrop_KeepExpanded`            | `Boolean` | `True`            | Keep `TreeNode`'s expanded. Usually, after dropping one `TreeNode` in to another, it stays collapsed
| `DragAndDrop_DragHandleColor`         | `Brush`   | `Brushes.DarkRed` | Color of the drag handles (arrow & line)

### Events:

#### DragAndDrop_CanDrag
Event to determinate if `TreeNode` can be dragged.
<br/>If not specified - it's possible to drag drag any `TreeNode`.
<br/>Example:
```vb
Public Class FormMain

   Private Sub TreeViewMain_DragAndDrop_CanDrag(ByRef data As Object)
      ' data.drag_node ' TreeNode that is getting dragged
      ' data.response  ' boolean to determinate if drag can happen. Default = True
      data.response = True
   End Sub
   
End Class
```

#### DragAndDrop_CanDrop
Event to determinate if `TreeNode` can be dropped on the `TreeNode`
currently bellow the mouse, while dragging.
<br/>If not specified - it's possible to drop on any `TreeNode`.
<br/>Example:
```vb
Public Class FormMain

   Private Sub TreeViewMain_DragAndDrop_CanDrop(ByRef data As Object)
      ' data.drag_node ' TreeNode that is getting dragged
      ' data.drop_node ' TreeNode that is currently under the mouse and would be dropped on, if mouse is released
      ' data.response  ' boolean to determinate if drop can happen. Default = True
      If data.drop_node.Text.Contains("File") Then data.response = False
   End Sub

End Class
```

#### DragAndDrop_OnDrop
Event that gets called when one `TreeNode` is dropped on to another
<br/>Example:
```vb
Public Class FormMain

   Private Sub TreeViewMain_DragAndDrop_OnDrop(data As Object) Handles TreeViewMain.DragAndDrop_OnDrop
      ' data.drag_node ' TreeNode that is getting dragged
      ' data.drop_node ' TreeNode that drag_node got dropped on. It will be 'Nothing' if dropped on root element
      If data.drop_node Is Nothing Then
         Debug.WriteLine("OnDrop: " + data.drag_node.Tag + " -> root")
      Else
         Debug.WriteLine("OnDrop: " + data.drag_node.Tag + " -> " + data.drop_node.Tag)
      End If
   End Sub

End Class
```



## LICENSE
You can use this plugin in any project you wish for free (personal and commercial) (open and closed source).
Only restriction is that you cannot sell my code as a plugin.
